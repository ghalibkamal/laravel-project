<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create() {
        return view('layouts.pertanyaan.create');
    }

    public function store(Request $request) {
       $request->validate([
           'judul' => 'required|unique:pertanyaan',
           'isi' => 'required'
       ]);
        //dd($request->all());
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Dikirim!');
    }

    public function index() {
        $pertanyaan = DB::table('pertanyaan')->get();
        return view('layouts.pertanyaan.index', compact('pertanyaan'));
    }
    
    public function show($id){
        $post = DB::table('pertanyaan')->where('id', $id)->first();
        
        return view('layouts.pertanyaan.show', compact('post'));
    }

    public function edit($id){
        $post = DB::table('pertanyaan')->where('id', $id)->first();
        
        return view('layouts.pertanyaan.edit', compact('post'));
    }

    public function update($id, Request $request){
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);
       
        $query = DB::table('pertanyaan')
                    -> where('id', $id)
                    ->update([
                        'judul' => $request['judul'],
                        'isi' => $request['isi']
                    ]);
        return redirect('/pertanyaan')->with('success', 'Anda Berhasil Menyunting Pertanyaan');

    }
    public function destroy($id){
        $query = DB::table('pertanyaan')->where ('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Anda Berhasil DIhapus');
    }
}
