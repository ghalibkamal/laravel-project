<html>
    <head>
        <title>Register</title>
        <body>
          <h1>Buat Account Baru!</h1>  
          <h2>Sign Up Form</h2>
          <form method="POST" action="/welcome">
          @csrf
            <label for="first_name">First Name:</label><br><br>
            <input type="text" name="first_name" id="first_name"><br><br>
            <label for="last_name">Last Name:</label><br><br>
            <input type="text" name="last_name" id="last_name">
            <br><br>
            <label>Gender:</label><br>
            <input type="radio" name="gender" value="0">Male<br>
            <input type="radio" name="gender" value="1">Female<br>
            <input type="radio" name="gender" value="2">Other
            <br><br>
            <label>Nationality</label><br><br>
            <select>
                <option value="idn">Indonesian</option>
                <option value="sgp">Singaporean</option>
                <option value="mys">Malaysian</option>
                <option value="aus">Australian</option>
            </select>
            <br><br>
            <label>Language Spoken</label><br><br>
            <input type="checkbox" name="language" value="0">Bahasa Indonesia<br>
            <input type="checkbox" name="language" value="1">English<br>
            <input type="checkbox" name="language" value="2">Other
            <br><br>
            <label for="bio">Bio:</label><br><br>
            <textarea cols="30" rows="10" id="bio"></textarea>
            <br><br>
            <input type="submit" value="Sign Up">
          </form>
        </body>
    </head>
</html>