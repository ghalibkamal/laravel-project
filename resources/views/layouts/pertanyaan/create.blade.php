@extends('layouts.master')

@section('content')
<div>
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Buat Pertanyaan Baru</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/pertanyaan" method="POST">
        @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="judul">Judul Pertanyaan</label>
        <input type="text" class="form-control" id="judul" name="judul" value="{{old('judul','')}}" placeholder="Masukan Judul Pertanyaan">
          @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
          <label for="isi">Isi Pertanyaan</label>
          <input type="text" class="form-control" id="isi" name="isi" value="{{old('isi','')}}" placeholder="isi">
          @error('isi')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Post</button>
      </div>
    </form>
  </div>
</div>
@endsection